<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
   
	$('button').click(function(){		
		$('button').hide();
		print();
		$('button').show();
	});

});
</script>

<style>

h2{
	color: green;
	padding-left: 40%;
}

p{
	font-family: Arial, Verdana, Tahoma, Sans-Serif;
	padding-left: 5%;
	line-height:40%;
}


hr{
	width: 50%;
	size: 10;
}

small{
	color: #006400;
}



.meio1{
	padding-left: 40%;
}

.meio2{
	padding-left: 25%;
}

.meio3{
	padding-left: 38%;
}

.meiop{
	text-align: center;
}


button{
	margin-left: 45%;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 20px;
	color: #079400;
	padding: 10px 20px;
	background: -moz-linear-gradient(
		top,
		#ffffff 0%,
		#ffffff 50%,
		#b5b5b5);
	background: -webkit-gradient(
		linear, left top, left bottom,
		from(#ffffff),
		color-stop(0.50, #ffffff),
		to(#b5b5b5));
	-moz-border-radius: 10px;
	-webkit-border-radius: 10px;
	border-radius: 10px;
	border: 3px solid #2ea100;
	-moz-box-shadow:
		6px 6px 15px rgba(000,000,000,0.5),
		inset 0px 0px 3px rgba(255,255,255,1);
	-webkit-box-shadow:
		6px 6px 15px rgba(000,000,000,0.5),
		inset 0px 0px 3px rgba(255,255,255,1);
	box-shadow:
		6px 6px 15px rgba(000,000,000,0.5),
		inset 0px 0px 3px rgba(255,255,255,1);
	text-shadow:
		1px -1px 0px rgba(13,12,13,0.2),
		0px 1px 0px rgba(255,255,255,1);
}


</style>

</head>
<body>

<button>Imprimir dados</button>
    
   <?php

    include 'simplexlsx.class.php';

    $xlsx = new SimpleXLSX('variaveis.xlsx');

   function saldar(){
		  $ano    = date('Y');
		  $dia    = date('d')-0;
		  $dsemana= date('w');
		  $data   = date('n');
		  $mes[1] ='Janeiro';
		  $mes[2] ='Fevereiro';
		  $mes[3] ='Março';
		  $mes[4] ='Abril';
		  $mes[5] ='Maio';
		  $mes[6] ='Junho';
		  $mes[7] ='Julho';
		  $mes[8] ='Agosto';
		  $mes[9] ='Setembro';
		  $mes[10]='Outubro';
		  $mes[11]='Novembro';
		  $mes[12]='Dezembro';
		  $semana[0] = 'Domingo';
		  $semana[1] = 'Segunda-Feira';
		  $semana[2] = 'Terça-Feira';
		  $semana[3] = 'Quarta-Feira';
		  $semana[4] = 'Quinta-Feira';
		  $semana[5] = 'Sexta-Feira';
		  $semana[6] = 'Sádado';
		  echo "<p class='meiop'>Florianópolis, ".$dia." de ".$mes[$data]." de ".$ano."</p>";
  	}
   
   
  
    //echo '<h1>$xlsx->rows()</h1>';
    //echo '<pre>';
    //print_r( $xlsx->rows() );
    //echo '</pre>';
    

    //echo $xlsx->rows()[1][0];
    
    
    //echo count($xlsx->rows());

    for($i =1;$i < count($xlsx->rows());$i++){



    	if(isset($xlsx->rows()[$i][0])){


    	$nome = $xlsx->rows()[$i][0];

    	$nomeCaixaAlta = strtoupper($nome);

    	$evento = $xlsx->rows()[$i][1];

    	$tituloEvento = $xlsx->rows()[$i][2];

    	$local =  $xlsx->rows()[$i][3];

    	

    	$dataEvento = $xlsx->rows()[$i][4];
    	$dataFormatada = ($dataEvento - 25569)*86400;
    	$dataCerta = date('d/m/Y',$dataFormatada);

    	$duracao = $xlsx->rows()[$i][5];

    	//$dataImpressao = $xlsx->rows()[$i][6]; //esse aqui não ira precisar, talvez.


    		
    		
    		echo '</br>';
    		echo "<img src='imagem/fiescVerde.jpg'/>";
			echo "</br>";	
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "<h2>D E C L A R A Ç Ã O</h2>";
			echo "</br>";	
			echo "</br>";
			echo "</br>";
			echo "<p>Declaro para os devidos fins, que o aluno <b>$nomeCaixaAlta</b>,</p>";
			echo "<p>regularmente matriculado nesta instituição de ensino, participou da $evento com </p>";
			echo "<p>tema <b>\"$tituloEvento\"</b> realizada no $local do SENAI Florianópolis,na em $dataCerta </p>";
			echo "<p>com duração de $duracao horas.</p>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";	
			echo "</br>";
			echo "</br>";		
			saldar();
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "<hr/>";
			echo "<p class='meio1'>Ronaldo Carlos Rohloff</p>";
			echo "<p class='meio2'>Coordenador do curso Superior em Automacao Industrial</p>";
			echo "<p class='meio1'>SENAI/SC - Florianópolis</p>";			
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			
			echo "<small>";
			echo "<p class='meio1'>SENAI/SC em Florianópolis</p>";
			echo "<p class='meio2'>Rod. SC 401, 3730 – Saco Grande – 88032-005 – Florianópolis – SC</p>";
			echo "<p class='meio2'>Fone: 48 3239 5800 – Fax: 48 3239 5802 – florianopolis@sc.senai.br</p>";
			echo "<p class='meio3'>0800 48 1212 – www.sc.senai.br</p>";
			echo "</small>";
			echo "</br>";	
			echo "</br>";
			echo "</br>";
    	}
    }
   
   ?>
   <button>Imprimir dados</button>
    
   

</body>
</html>
