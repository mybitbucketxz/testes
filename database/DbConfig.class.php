<?php


class DbConfig{
    
    
    private $Usuario;
    private $Senha;
    private $Hostname;
    private $Database;
    
    
    public function __construct($usuario,$senha,$hostname,$database) {
        $this->Usuario = $usuario;
        $this->Senha = $senha;
        $this->Hostname = $hostname;
        $this->Database = $database;
    }
    
    public function ConectarDb(){
        $conectar = mysqli_connect($this->Hostname, $this->Usuario, $this->Senha, $this->Database);
        
        if($conectar){
            return $conectar;
        }else{
            return false;
        }
    }
    
    
}


?>
