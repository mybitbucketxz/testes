<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<link rel="stylesheet" href="mestre.css"> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script>
$(function(){

	$("input[name='file']").on("change",function(){

		var formData = new FormData($("#formulario")[0]);
		var url = "uploadArquivo2.php";

		$.ajax({

			url: url,
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			success: function(datos){
				//alert(datos);
				$("#conteudo").html(datos);
			}
		});


	});

	//Esse metodo live é utilizado quando se quer aplicar os eventos Jquery
	//Em novos elementos adicionados na pagina via ajax.
	$("button").live('click', function(){ 
		//alert('botão clicado!');		
		$("button").hide();
		$("form").hide();
		print();
		$("form").show();
		$("button").show();
		
	});


});
</script>

<style>
    
    .justificado{
        text-align: justify;
        line-height:110%;
		margin-left: 15%;
    }

    h2{
        color: green;
    }
    
    small{
	color: #006400;
    }

   
   
button{
	margin-left: 45%;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 20px;
	color: #079400;
	padding: 10px 20px;
	background: -moz-linear-gradient(
		top,
		#ffffff 0%,
		#ffffff 50%,
		#b5b5b5);
	background: -webkit-gradient(
		linear, left top, left bottom,
		from(#ffffff),
		color-stop(0.50, #ffffff),
		to(#b5b5b5));
	-moz-border-radius: 10px;
	-webkit-border-radius: 10px;
	border-radius: 10px;
	border: 3px solid #2ea100;
	-moz-box-shadow:
		6px 6px 15px rgba(000,000,000,0.5),
		inset 0px 0px 3px rgba(255,255,255,1);
	-webkit-box-shadow:
		6px 6px 15px rgba(000,000,000,0.5),
		inset 0px 0px 3px rgba(255,255,255,1);
	box-shadow:
		6px 6px 15px rgba(000,000,000,0.5),
		inset 0px 0px 3px rgba(255,255,255,1);
	text-shadow:
		1px -1px 0px rgba(13,12,13,0.2),
		0px 1px 0px rgba(255,255,255,1);
}


input{

	margin-left: 35%;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 20px;
	color: #079400;
	padding: 10px 20px;	
	background: -moz-linear-gradient(
		top,
		#ffffff 0%,
		#ffffff 50%,
		#b5b5b5);
	background: -webkit-gradient(
		linear, left top, left bottom,
		from(#ffffff),
		color-stop(0.50, #ffffff),
		to(#b5b5b5));
	-moz-border-radius: 10px;
	-webkit-border-radius: 10px;
	border-radius: 10px;
	border: 3px solid #3CB371;
	-moz-box-shadow:
		6px 6px 15px rgba(000,000,000,0.5),
		inset 0px 0px 3px rgba(255,255,255,1);
	-webkit-box-shadow:
		6px 6px 15px rgba(000,000,000,0.5),
		inset 0px 0px 3px rgba(255,255,255,1);
	box-shadow:
		6px 6px 15px rgba(000,000,000,0.5),
		inset 0px 0px 3px rgba(255,255,255,1);
	text-shadow:
		1px -1px 0px rgba(13,12,13,0.2),
		0px 1px 0px rgba(255,255,255,1);
    }
    
    
    /* 

</style>

</head>
<body>


	</br>
	<form method="POST" id="formulario" enctype="multipart/form-data">
    	<input type="file" name="file">
    </form>
    </br>
    <div id="conteudo"></div>
   

</body>
</html>
