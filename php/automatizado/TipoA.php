﻿<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TipoA
 *
 * @author willian_rosa
 */

class TipoA extends GeradorImpressao{
    
  
    
    public function __construct($nomeDoArquivoExcelComAExtencao) {
        parent::__construct($nomeDoArquivoExcelComAExtencao);       
    }
    
    
    public function getObjExcel() {
        return parent::getObjExcel();
    }

    public function setObjExcel($nomeDoArquivoExcelComAExtencao) {
        parent::setObjExcel($nomeDoArquivoExcelComAExtencao);
    }

    
    
       
    public function imprimirDataAtual() {
        parent::imprimirDataAtual();
    }

    
    public function imprimirTodoExcel() {
        
         //$xlsx = new SimpleXLSX($this->getNomeDoArquivoExcelComAExtencao());
        
        $xlsx = $this->getObjExcel();
          
         for($i =1;$i < count($xlsx->rows());$i++){


    	if(isset($xlsx->rows()[$i][0])&&!empty($xlsx->rows()[$i][0])){


    	$nome = $xlsx->rows()[$i][0];

    	$nomeCaixaAlta = strtoupper($nome);

    	$evento = $xlsx->rows()[$i][1];

    	$tituloEvento = $xlsx->rows()[$i][2];

    	$local =  $xlsx->rows()[$i][3];

    	

    	$dataEvento = $xlsx->rows()[$i][4];
    	$dataFormatada = ($dataEvento - 25569)*86400;
    	$dataCerta = date('d/m/Y',$dataFormatada);

    	$duracao = $xlsx->rows()[$i][5];
        
        $nomeCordenador = $xlsx->rows()[$i][6];
        
        $atribuicaoCordenador = $xlsx->rows()[$i][7];

    	//$dataImpressao = $xlsx->rows()[$i][6]; //esse aqui não ira precisar, talvez.   		
    		
           echo "<div class='w3-row'>";
           echo "<div class='w3-container w3-center'>";                    
            echo "<img src='imagens/fiescVerde.jpg'/>";
            echo "</br>";	
			
			echo "</br>";
			echo "</br>";
			echo "<h2><b>D E C L A R A Ç Ã O</b></h2>";
			echo "</br>";	
			echo "</br>";
			
                        echo "<div width='100%'>";
			echo "<p class='justificado'>Declaro para os devidos fins, que o aluno <b>$nomeCaixaAlta</b>,</p>";
			echo "<p class='justificado'>regularmente matriculado nesta instituição de ensino, participou da $evento com </p>";
			echo "<p class='justificado'>tema <b>\"$tituloEvento\"</b> realizada no $local do SENAI Florianópolis,</p>";
			echo "<p class='justificado'>na em $dataCerta  com duração de $duracao.</p>";
                        echo "</div>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
					
			$this->imprimirDataAtual();
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			
			
                        echo "<div class='m2'>";
			echo "<hr width='50%'size='1'/>";
                        echo "</div>";
			echo "<p class='meio1'>".$nomeCordenador."</p>";
			echo "<p class='meio2'>".$atribuicaoCordenador."</p>";
			echo "<p class='meio1'>SENAI/SC - Florianópolis</p>";			
			echo "</br>";
			echo "</br>";
			echo "</br>";
			
			
			
			echo "<small>";
			echo "<p class='meio1'>SENAI/SC em Florianópolis</p>";
			echo "<p class='meio2'>Rod. SC 401, 3730 – Saco Grande – 88032-005 – Florianópolis – SC</p>";
			echo "<p class='meio2'>Fone: 48 3239 5800 – Fax: 48 3239 5802 – florianopolis@sc.senai.br</p>";
			echo "<p class='meio3'>0800 48 1212 – www.sc.senai.br</p>";
			echo "</small>";
			
			
			
           echo "</div>";
          echo "</div>";
                
			
    	}
      }
   }

   
   public function imprimirExemplo() {
                echo '</br>';
    		echo "<img src='imagens/fiescVerde.jpg'/>";
			echo "</br>";	
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "<h2>D E C L A R A Ç Ã O</h2>";
			echo "</br>";	
			echo "</br>";
			echo "</br>";
			echo "<p>Declaro para os devidos fins, que o aluno <b>JOSE ANTONIO SIQUEIRA</b>,</p>";
			echo "<p>regularmente matriculado nesta instituição de ensino, participou da palestra com </p>";
			echo "<p>tema <b>\"Drogas\"</b> realizada no auditorio do SENAI Florianópolis,na em 13/01/2017 </p>";
			echo "<p>com duração de 1:50 horas.</p>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";	
			echo "</br>";
			echo "</br>";		
			$this->imprimirDataAtual();
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "<hr/>";
			echo "<p class='meio1'>Ronaldo Carlos Rohloff</p>";
			echo "<p class='meio2'>Coordenador do curso Superior em Automacao Industrial</p>";
			echo "<p class='meio1'>SENAI/SC - Florianópolis</p>";			
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			echo "</br>";
			
			echo "<small>";
			echo "<p class='meio1'>SENAI/SC em Florianópolis</p>";
			echo "<p class='meio2'>Rod. SC 401, 3730 – Saco Grande – 88032-005 – Florianópolis – SC</p>";
			echo "<p class='meio2'>Fone: 48 3239 5800 – Fax: 48 3239 5802 – florianopolis@sc.senai.br</p>";
			echo "<p class='meio3'>0800 48 1212 – www.sc.senai.br</p>";
			echo "</small>";
			echo "</br>";	
			echo "</br>";
			echo "</br>";
     }

   
    public function imprimirVariaveis() {
        echo "<h3>1 - Nome do aluno</h3>";
        echo "<h3>2 - Evento(Palestra,visita..)</h3>";
        echo "<h3>3 - Titulo do evento</h3>";
        echo "<h3>4 - Local</h3>";
        echo "<h3>5 - Data do evento</h3>";
        echo "<h3>6 - Duracão</h3>";
    }

    
}


?>