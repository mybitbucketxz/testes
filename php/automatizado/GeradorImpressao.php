<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GeradorImpressao
 *
 * @author willian_rosa
 */

include 'simplexlsx.class.php';
abstract class GeradorImpressao {
        
   
    private $objExcel;
   
    
    function __construct($nomeDoArquivoExcelComAExtencao) {       
        $this->objExcel =  new SimpleXLSX($nomeDoArquivoExcelComAExtencao);        
    }
    
    
   function getObjExcel() {
       return $this->objExcel;
   }

   function setObjExcel($nomeDoArquivoExcelComAExtencao) {
       $this->objExcel = new SimpleXLSX($nomeDoArquivoExcelComAExtencao);
   }

       

        
    abstract  function imprimirTodoExcel();    
   
    abstract  function imprimirExemplo();
    
    abstract  function imprimirVariaveis();
    
    
    
    function imprimirDataAtual(){
                  $ano    = date('Y');
		  $dia    = date('d')-0;
		  $dsemana= date('w');
		  $data   = date('n');
		  $mes[1] ='Janeiro';
		  $mes[2] ='Fevereiro';
		  $mes[3] ='Março';
		  $mes[4] ='Abril';
		  $mes[5] ='Maio';
		  $mes[6] ='Junho';
		  $mes[7] ='Julho';
		  $mes[8] ='Agosto';
		  $mes[9] ='Setembro';
		  $mes[10]='Outubro';
		  $mes[11]='Novembro';
		  $mes[12]='Dezembro';
		  $semana[0] = 'Domingo';
		  $semana[1] = 'Segunda-Feira';
		  $semana[2] = 'Terça-Feira';
		  $semana[3] = 'Quarta-Feira';
		  $semana[4] = 'Quinta-Feira';
		  $semana[5] = 'Sexta-Feira';
		  $semana[6] = 'Sádado';
		  echo "<p class='meiop'>Florianópolis, ".$dia." de ".$mes[$data]." de ".$ano."</p>";
    }
    
   

}
